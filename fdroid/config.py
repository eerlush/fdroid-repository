repo_url = "https://lucaapp.gitlab.io/fdroid-repository/fdroid/repo"
repo_name = "Culture4Life"
repo_icon = "fdroid-icon.png"
repo_description = """
Official repository for the luca app.
"""
archive_older = 0
local_copy_dir = "/fdroid"
keystore = "../keystore.jks"
repo_keyalias = "fdroid"
